package com.testcat.kemnhukham.http.service.appkode4;


import com.testcat.kemnhukham.data.model.Agent;
import com.testcat.kemnhukham.data.wrapper.AgentWrapper;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by kodeartisan on 30/07/17.
 */

public interface AgentService {

    @POST("agent/create")
    Observable<AgentWrapper> create(@Body Agent agent);
}
