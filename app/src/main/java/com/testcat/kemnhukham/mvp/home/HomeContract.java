package com.testcat.kemnhukham.mvp.home;

import com.testcat.kemnhukham.base.BaseContract;
import com.testcat.kemnhukham.data.model.App;
import com.testcat.kemnhukham.data.model.Paket;

import java.util.List;
import java.util.Map;

/**
 * Created by kodeartisan on 24/08/17.
 */

public interface HomeContract {

    interface View extends BaseContract.BaseView {
        void onGetAppList(List<App> apps);
        void onGetPaketList(List<Paket> paketList);

    }

    interface Presenter<T> extends BaseContract.BasePresenter<T> {
        void initializeData();
        void getAppList(Map<String, String> options);
        void getPaketList();
    }
}
