package com.testcat.kemnhukham.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by kodeartisan on 21/09/17.
 */

public class Paket implements Parcelable {

    private String title;
    private List<Soal> soalList;

    public Paket(String title, List<Soal> soalList) {
        this.title = title;
        this.soalList = soalList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Soal> getSoalList() {
        return soalList;
    }

    public void setSoalList(List<Soal> soalList) {
        this.soalList = soalList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeTypedList(this.soalList);
    }

    protected Paket(Parcel in) {
        this.title = in.readString();
        this.soalList = in.createTypedArrayList(Soal.CREATOR);
    }

    public static final Parcelable.Creator<Paket> CREATOR = new Parcelable.Creator<Paket>() {
        @Override
        public Paket createFromParcel(Parcel source) {
            return new Paket(source);
        }

        @Override
        public Paket[] newArray(int size) {
            return new Paket[size];
        }
    };
}
