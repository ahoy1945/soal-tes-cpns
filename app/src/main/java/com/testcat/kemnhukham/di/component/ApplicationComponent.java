package com.testcat.kemnhukham.di.component;

import com.testcat.kemnhukham.http.service.IfcfgService;
import com.testcat.kemnhukham.data.repository.appkode4.agent.IAgentRepository;
import com.testcat.kemnhukham.data.repository.appkode4.app.IAppRepository;
import com.testcat.kemnhukham.di.module.ApplicationModule;
import com.testcat.kemnhukham.di.module.DataModule;
import com.testcat.kemnhukham.di.module.NetworkModule;
import com.testcat.kemnhukham.rx.RxBus;
import com.testcat.kemnhukham.rx.scheduler.BaseSchedulerProvider;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by kodeartisan on 23/08/17.
 */
@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class, DataModule.class})
public interface ApplicationComponent {

    BaseSchedulerProvider getSchedulerProvider();

    IAppRepository getIAppRepository();

    IAgentRepository getIAgentRepository();

    IfcfgService getIfcfgService();

    RxBus getRxBus();
}
