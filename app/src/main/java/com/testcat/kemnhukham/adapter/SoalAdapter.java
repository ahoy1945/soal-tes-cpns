package com.testcat.kemnhukham.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.testcat.kemnhukham.R;
import com.testcat.kemnhukham.data.model.Soal;

import java.util.List;

/**
 * Created by kodeartisan on 22/09/17.
 */

public class SoalAdapter extends BaseQuickAdapter<Soal, BaseViewHolder> {

    public static final String TAG = SoalAdapter.class.getSimpleName();

    public SoalAdapter(@LayoutRes int layoutResId, @Nullable List<Soal> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Soal item) {
        helper.setText(R.id.title, item.getTitle());
    }
}
