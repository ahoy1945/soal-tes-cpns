package com.testcat.kemnhukham.mvp.soal;

import android.support.annotation.NonNull;

import com.testcat.kemnhukham.data.model.Paket;
import com.testcat.kemnhukham.data.repository.appkode4.app.IAppRepository;
import com.testcat.kemnhukham.mvp.home.HomeContract;
import com.testcat.kemnhukham.rx.RxPresenter;
import com.testcat.kemnhukham.rx.scheduler.BaseSchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by kodeartisan on 22/09/17.
 */

public class SoalPresenter extends RxPresenter<SoalContract.View> implements SoalContract.Presenter<SoalContract.View>  {

    public static final String TAG = SoalPresenter.class.getSimpleName();

    private BaseSchedulerProvider mSchedulerProvider;
    private SoalContract.View mView;

    private List<Paket> mPaketList = new ArrayList<>();

    @Inject
    public SoalPresenter(@NonNull BaseSchedulerProvider schedulerProvider) {

        this.mSchedulerProvider = schedulerProvider;

    }
}
