package com.testcat.kemnhukham.data.repository.appkode4.agent;

import android.support.annotation.NonNull;


import com.testcat.kemnhukham.data.model.Agent;
import com.testcat.kemnhukham.http.service.appkode4.AgentService;
import com.testcat.kemnhukham.data.wrapper.AgentWrapper;
import com.testcat.kemnhukham.rx.scheduler.BaseSchedulerProvider;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by kodeartisan on 30/07/17.
 */


public class RemoteAgentImpl implements IAgentRepository {

    public static final String TAG = RemoteAgentImpl.class.getSimpleName();

    private final AgentService agentService;
    private final BaseSchedulerProvider schedulerProvider;

    @Inject
    public RemoteAgentImpl(@NonNull AgentService agentService, @NonNull BaseSchedulerProvider schedulerProvider) {
        this.agentService = agentService;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    public Observable<Agent> create(Agent agent) {

         Observable<Agent> agentObservable = agentService
                .create(agent)
                .subscribeOn(schedulerProvider.multi())
                .map(AgentWrapper::getAgent);

        return agentObservable;
    }


}
