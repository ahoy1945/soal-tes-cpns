package com.testcat.kemnhukham.mvp.home;

import com.testcat.kemnhukham.R;
import com.testcat.kemnhukham.base.BaseFragment;

/**
 * Created by kodeartisan on 22/09/17.
 */

public class PaketFragment extends BaseFragment<HomePresenter> {

    public static final String TAG = PaketFragment.class.getSimpleName();

    @Override
    public void initDependencies() {
        getFragmentComponent().inject(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_paket;
    }
}
