package com.testcat.kemnhukham.di.module;

import com.testcat.kemnhukham.data.repository.appkode4.agent.IAgentRepository;
import com.testcat.kemnhukham.data.repository.appkode4.agent.RemoteAgentImpl;
import com.testcat.kemnhukham.data.repository.appkode4.app.IAppRepository;
import com.testcat.kemnhukham.data.repository.appkode4.app.RemoteAppImpl;
import com.testcat.kemnhukham.data.repository.appkode4.image.IImageRepository;
import com.testcat.kemnhukham.data.repository.appkode4.image.RemoteImageImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by kodeartisan on 24/08/17.
 */
@Module
public class DataModule {

    @Singleton
    @Provides
    IImageRepository provideImageRepository(RemoteImageImpl remoteImage) {
        return remoteImage;
    }

    @Singleton
    @Provides
    IAppRepository provideAppRepository(RemoteAppImpl remoteApp) {
        return remoteApp;
    }

    @Singleton
    @Provides
    IAgentRepository provideAgentRepository(RemoteAgentImpl remoteAgent) {
        return remoteAgent;
    }
}
