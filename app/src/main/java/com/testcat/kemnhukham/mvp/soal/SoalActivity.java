package com.testcat.kemnhukham.mvp.soal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.testcat.kemnhukham.R;
import com.testcat.kemnhukham.adapter.SoalAdapter;
import com.testcat.kemnhukham.base.BaseActivityMvp;
import com.testcat.kemnhukham.base.BaseApplication;
import com.testcat.kemnhukham.data.model.Paket;
import com.testcat.kemnhukham.data.model.Soal;
import com.testcat.kemnhukham.di.component.DaggerActivityComponent;
import com.testcat.kemnhukham.mvp.quiz.QuizActivity;
import com.testcat.kemnhukham.util.AdmobUtil;
import com.testcat.kemnhukham.util.AppUtil;
import com.testcat.kemnhukham.util.LogUtil;
import com.testcat.kemnhukham.util.PreferencesUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class SoalActivity extends BaseActivityMvp<SoalPresenter> implements SoalContract.View, BaseQuickAdapter.OnItemClickListener {

    public static final String TAG = SoalActivity.class.getSimpleName();

    private static Paket mPaket;
    private static List<Soal> mSoalList = new ArrayList<>();
    private SoalAdapter mSoalAdapter;
    private InterstitialAd mInterstitialAd;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerview;

    @BindView(R.id.title)
    TextView mTitle;



    @Override
    protected int getLayoutId() {
        return R.layout.activity_soal;
    }

    @Override
    protected void injectDependencies() {
        DaggerActivityComponent.builder()
                .applicationComponent(BaseApplication.getAppComponent())
                .build().inject(this);
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        mInterstitialAd = AdmobUtil.loadInterstitial(this, new InterstitialAd(this));

        mPresenter.attachView(this);
        mPaket = getIntent().getParcelableExtra("paket");
        if(mPaket != null) {
            mTitle.setText(mPaket.getTitle());
            mSoalList = mPaket.getSoalList();
        }

        initRecyclerview();
    }

    private void initRecyclerview() {
        mSoalAdapter = new SoalAdapter(R.layout.adapter_soal, mSoalList);
        mSoalAdapter.setOnItemClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mRecyclerview.setLayoutManager(linearLayoutManager);
        mRecyclerview.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mRecyclerview.setAdapter(mSoalAdapter);
        mRecyclerview.setHasFixedSize(true);
    }


    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                goToQuizActivity(position);
            }
        });

        if(AdmobUtil.increaseAdmobValue() >= AdmobUtil.ARG_TRIGGER_VALUE) {
            AdmobUtil.resetAdmobValue();
            if(mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();Toast.makeText(this, "Loaded", Toast.LENGTH_SHORT).show();
            } else {
                mInterstitialAd = AdmobUtil.loadInterstitial(this, new InterstitialAd(this));
                mInterstitialAd.setAdListener(new AdListener(){
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        goToQuizActivity(position);
                    }

                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mInterstitialAd.show();
                    }
                });
            }
        } else {
            goToQuizActivity(position);
        }
        //Toast.makeText(this, mPaket.getSoalList().get(position).getUrl(), Toast.LENGTH_SHORT).show();
    }

    @OnClick({R.id.button_rate})
    public void OnClickedButton(View view) {
        switch (view.getId()) {
            case R.id.button_rate:
                AppUtil.goToMarket(getPackageName());
                break;
        }
    }

    private void goToQuizActivity(int position) {

        Intent intent = new Intent(this, QuizActivity.class);
        intent.putExtra("soal", mSoalList.get(position));
        startActivity(intent);
    }
}
