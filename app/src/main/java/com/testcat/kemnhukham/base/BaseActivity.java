package com.testcat.kemnhukham.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.testcat.kemnhukham.R;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by kodeartisan on 23/08/17.
 */

public abstract class BaseActivity<T extends BaseContract.BasePresenter> extends RxAppCompatActivity implements BaseContract.BaseView {

    public static final String TAG = BaseActivity.class.getSimpleName();

    protected Unbinder unbinder;
    private ConstraintLayout mError;

    protected abstract @LayoutRes int getLayoutId();

    protected abstract void injectDependencies();

    protected abstract void initData(Bundle savedInstanceState);


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies();
        setContentView(getLayoutId());
        unbinder = ButterKnife.bind(this);
        mError = ButterKnife.findById(this, R.id.cl_error);
        initData(savedInstanceState);
        initWidget();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(unbinder != null) unbinder.unbind();


    }

    protected void initWidget() {
    }

    protected void loadData() {
    }

    protected void initDatas() {
        loadData();
    }

    protected void initVariables() {
    }

    @Override
    public void showError(String msg) {
        if (mError != null) {
            visible(mError);
        }
    }

    @Override
    public void complete() {
        if (mError != null) {
            gone(mError);
        }
    }

    protected void gone(final View... views) {
        if (views != null && views.length > 0) {
            for (View view : views) {
                if (view != null) {
                    view.setVisibility(View.GONE);
                }
            }
        }
    }

    protected void visible(final View... views) {
        if (views != null && views.length > 0) {
            for (View view : views) {
                if (view != null) {
                    view.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    protected void gone(final @IdRes int... id) {
        if (id != null && id.length > 0) {
            for (int resId : id) {
                View view = $(resId);
                if (view != null)
                    gone(view);
            }
        }

    }

    protected void visible(final @IdRes int... id) {
        if (id != null && id.length > 0) {
            for (int resId : id) {
                View view = $(resId);
                if (view != null)
                    visible(view);
            }
        }
    }

    private View $(@IdRes int id) {
        View view;
        if (this != null) {
            view = this.findViewById(id);
            return view;
        }
        return null;
    }
}
