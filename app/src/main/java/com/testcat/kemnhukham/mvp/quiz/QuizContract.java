package com.testcat.kemnhukham.mvp.quiz;

import com.testcat.kemnhukham.base.BaseContract;

/**
 * Created by kodeartisan on 22/09/17.
 */

public interface QuizContract {

    interface View extends BaseContract.BaseView {


    }

    interface Presenter<T> extends BaseContract.BasePresenter<T> {

    }
}
