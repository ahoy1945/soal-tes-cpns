package com.testcat.kemnhukham.data.wrapper;

import com.google.gson.annotations.SerializedName;
import com.testcat.kemnhukham.data.model.Image;

import java.util.List;

/**
 * Created by kodeartisan on 29/07/17.
 */

public class ImageWrapper {

    @SerializedName("data")
    private List<Image> images;

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
}
