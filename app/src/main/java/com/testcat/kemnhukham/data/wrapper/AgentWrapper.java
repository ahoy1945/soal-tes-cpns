package com.testcat.kemnhukham.data.wrapper;

import com.google.gson.annotations.SerializedName;
import com.testcat.kemnhukham.data.model.Agent;

/**
 * Created by kodeartisan on 30/07/17.
 */

public class AgentWrapper {

    @SerializedName("data")
    private Agent agent;

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }
}
