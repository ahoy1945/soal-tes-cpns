package com.testcat.kemnhukham.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kodeartisan on 21/09/17.
 */

public class Soal implements Parcelable {

    private String title;
    private String url;

    public Soal(String title, String url) {
        this.title = title;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.url);
    }

    protected Soal(Parcel in) {
        this.title = in.readString();
        this.url = in.readString();
    }

    public static final Parcelable.Creator<Soal> CREATOR = new Parcelable.Creator<Soal>() {
        @Override
        public Soal createFromParcel(Parcel source) {
            return new Soal(source);
        }

        @Override
        public Soal[] newArray(int size) {
            return new Soal[size];
        }
    };
}
