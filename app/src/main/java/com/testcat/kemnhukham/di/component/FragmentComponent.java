package com.testcat.kemnhukham.di.component;

import android.app.Activity;

import com.testcat.kemnhukham.di.module.ApplicationModule;
import com.testcat.kemnhukham.di.module.FragmentModule;
import com.testcat.kemnhukham.di.module.PresenterModule;
import com.testcat.kemnhukham.di.scope.FragmentScope;
import com.testcat.kemnhukham.mvp.home.PaketFragment;

import dagger.Component;

/**
 * Created by kodeartisan on 22/09/17.
 */

@FragmentScope
@Component(dependencies = ApplicationComponent.class, modules = {FragmentModule.class, PresenterModule.class})
public interface FragmentComponent {

    Activity getActivity();

    void inject(PaketFragment paketFragment);
}
