package com.testcat.kemnhukham.di.module;

import android.support.annotation.NonNull;

import com.testcat.kemnhukham.data.repository.appkode4.app.IAppRepository;
import com.testcat.kemnhukham.di.scope.ActivityScope;
import com.testcat.kemnhukham.mvp.home.HomeContract;
import com.testcat.kemnhukham.mvp.home.HomePresenter;
import com.testcat.kemnhukham.mvp.quiz.QuizContract;
import com.testcat.kemnhukham.mvp.quiz.QuizPresenter;
import com.testcat.kemnhukham.mvp.soal.SoalContract;
import com.testcat.kemnhukham.mvp.soal.SoalPresenter;
import com.testcat.kemnhukham.rx.scheduler.BaseSchedulerProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Created by kodeartisan on 24/08/17.
 */

@Module
public class PresenterModule {

    @ActivityScope
    @Provides
    HomeContract.Presenter homePresenter(@NonNull BaseSchedulerProvider schedulerProvider, @NonNull IAppRepository appRepository) {
        return new HomePresenter(schedulerProvider, appRepository);
    }

    @ActivityScope
    @Provides
    SoalContract.Presenter soalPresenter(@NonNull BaseSchedulerProvider schedulerProvider) {
        return new SoalPresenter(schedulerProvider);
    }

    @ActivityScope
    @Provides
    QuizContract.Presenter quizPresenter(@NonNull BaseSchedulerProvider schedulerProvider) {
        return new QuizPresenter(schedulerProvider);
    }
}
