package com.testcat.kemnhukham.mvp.soal;

import com.testcat.kemnhukham.base.BaseContract;

/**
 * Created by kodeartisan on 22/09/17.
 */

public interface SoalContract {

    interface View extends BaseContract.BaseView {


    }

    interface Presenter<T> extends BaseContract.BasePresenter<T> {

    }
}
