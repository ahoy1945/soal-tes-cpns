package com.testcat.kemnhukham.di.component;

import com.testcat.kemnhukham.di.module.PresenterModule;
import com.testcat.kemnhukham.di.scope.ActivityScope;
import com.testcat.kemnhukham.mvp.home.HomeActivity;
import com.testcat.kemnhukham.mvp.quiz.QuizActivity;
import com.testcat.kemnhukham.mvp.soal.SoalActivity;

import dagger.Component;

/**
 * Created by kodeartisan on 24/08/17.
 */
@ActivityScope
@Component(dependencies = ApplicationComponent.class, modules = PresenterModule.class)
public interface ActivityComponent {

    void inject(HomeActivity activity);
    void inject(SoalActivity activity);
    void inject(QuizActivity activity);
}
