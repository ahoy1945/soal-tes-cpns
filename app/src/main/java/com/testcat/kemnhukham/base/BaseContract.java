package com.testcat.kemnhukham.base;

/**
 * Created by kodeartisan on 24/08/17.
 */

public interface BaseContract {

    interface BaseView {

        void showError(String msg);

        void complete();


    }

    interface BasePresenter<T>  {


        void attachView(T view);


        void detachView();
    }

}
