package com.testcat.kemnhukham.util;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.testcat.kemnhukham.R;
import com.testcat.kemnhukham.listener.InterstitialListener;

/**
 * Created by kodeartisan on 19/09/17.
 */

public class AdmobUtil {

    public static final int ARG_GONE = 0;
    public static final int ARG_DEBUGGING = 1;
    public static final String ARG_PREF_KEY = "ADS_SHOW";

    // For every recipe detail you want to display interstitial ad
    public static final int ARG_TRIGGER_VALUE = 3;
    // Admob visibility parameter. set 1 to show admob and 0 to hide.
    public static final int ARG_ADMOB_VISIBILITY = 1;
    // Set value to 1 if you are still in development process, and zero if you are ready to publish the app.
    public static final int ARG_ADMOB_DEVELOPMENT_TYPE = 1;


    public static void loadBanner(final AdView ads) {
        AdRequest adRequest;

        if(ARG_ADMOB_DEVELOPMENT_TYPE == ARG_DEBUGGING) {
            adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();

        } else {
            adRequest = new AdRequest.Builder().build();
        }

        ads.loadAd(adRequest);

        ads.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(int i) {

                super.onAdFailedToLoad(i);
                Log.d(AdmobUtil.class.getSimpleName(), "BANNER FAILED TO LOAD");
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.d(AdmobUtil.class.getSimpleName(), "BANNER LOADED");
                if(ads != null) {
                    ads.setVisibility(View.VISIBLE);
                }
            }
        });


    }

    public static boolean adVisibility(AdView ads, int paremeter) {
        ads.setVisibility(paremeter);

        if(paremeter == ARG_GONE) {
            ads.setVisibility(View.INVISIBLE);
            return false;
        } else {
            ads.setVisibility(View.VISIBLE);
            return true;
        }
    }

    public static InterstitialAd loadInterstitial(Context c, final InterstitialAd interstitialAd) {
        interstitialAd.setAdUnitId(c.getResources().getString(R.string.admob_interstitial));

        AdRequest adRequest = new AdRequest.Builder().build();

        interstitialAd.loadAd(adRequest);
        //interstitialAd.setAdListener(adListener);

        return interstitialAd;
    }

    public static InterstitialAd showInterstitial(InterstitialAd mInterstitialAd, AdListener listener, Context context) {


        return mInterstitialAd;

    }

    public static int increaseAdmobValue() {
        int oldAdmobValue = PreferencesUtil.getInt(AdmobUtil.ARG_PREF_KEY, 0);
        PreferencesUtil.putInt(AdmobUtil.ARG_PREF_KEY, oldAdmobValue + 1 );
        int newAdmobValue = PreferencesUtil.getInt(AdmobUtil.ARG_PREF_KEY, 0);

        return newAdmobValue;
    }

    public static void resetAdmobValue() {
        PreferencesUtil.putInt(AdmobUtil.ARG_PREF_KEY, 0);
    }

    public static AdRequest loadBannerView(AdView adView) {
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        adView.loadAd(adRequest);

        return adRequest;
    }
}
