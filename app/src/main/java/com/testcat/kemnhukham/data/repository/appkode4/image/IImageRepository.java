package com.testcat.kemnhukham.data.repository.appkode4.image;


import com.testcat.kemnhukham.data.model.Image;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by kodeartisan on 06/08/17.
 */

public interface IImageRepository {

    Observable<List<Image>> get(Map<String, String> options);
}
