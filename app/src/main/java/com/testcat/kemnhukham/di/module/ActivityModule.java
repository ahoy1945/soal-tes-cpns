package com.testcat.kemnhukham.di.module;

import android.app.Activity;

import com.testcat.kemnhukham.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by kodeartisan on 24/08/17.
 */

@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityScope
    public Activity proviceActivity() {
        return mActivity;
    }
}
