package com.testcat.kemnhukham.http.service.appkode4;

import com.testcat.kemnhukham.data.wrapper.AppListWrapper;
import com.testcat.kemnhukham.data.wrapper.AppWrapper;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by kodeartisan on 29/07/17.
 */

public interface AppService {

    @GET("app/{id}/detail")
    Observable<AppWrapper> queryAppDetail(@Path("id") String id);

    @GET("app")
    Observable<AppListWrapper> queryAppList(@QueryMap Map<String, String> options);
}
