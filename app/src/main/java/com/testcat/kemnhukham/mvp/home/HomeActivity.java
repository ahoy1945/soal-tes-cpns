package com.testcat.kemnhukham.mvp.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.avocarrot.sdk.mediation.ResponseStatus;
import com.avocarrot.sdk.nativead.NativeAd;
import com.avocarrot.sdk.nativead.listeners.StreamNativeAdCallback;
import com.avocarrot.sdk.nativead.recyclerview.StreamAdRecyclerAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.testcat.kemnhukham.R;
import com.testcat.kemnhukham.adapter.AppsAdapter;
import com.testcat.kemnhukham.adapter.PaketAdapter;
import com.testcat.kemnhukham.base.BaseActivityMvp;
import com.testcat.kemnhukham.base.BaseAdapter;
import com.testcat.kemnhukham.base.BaseApplication;
import com.testcat.kemnhukham.constant.AppConstant;
import com.testcat.kemnhukham.data.model.App;
import com.testcat.kemnhukham.data.model.Paket;
import com.testcat.kemnhukham.data.model.Soal;
import com.testcat.kemnhukham.di.component.DaggerActivityComponent;
import com.testcat.kemnhukham.listener.InterstitialListener;
import com.testcat.kemnhukham.mvp.soal.SoalActivity;
import com.testcat.kemnhukham.util.AdmobUtil;
import com.testcat.kemnhukham.util.AppUtil;
import com.testcat.kemnhukham.util.LogUtil;
import com.testcat.kemnhukham.util.PreferencesUtil;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;


public class HomeActivity extends BaseActivityMvp<HomeContract.Presenter> implements HomeContract.View, BaseQuickAdapter.OnItemClickListener{

    public static final String TAG = HomeActivity.class.getSimpleName();

    private StartAppAd startAppAd = new StartAppAd(this);
    private List<App> appList = new ArrayList<>();
    private List<Paket> mPaketList = new ArrayList<>();
    private AppsAdapter mAppsAdapter;
    private PaketAdapter mPaketAdapter;
    private InterstitialAd mInterstitialAd;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.nav_view)
    NavigationView mNavigationView;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerview;

    @BindView(R.id.title)
    TextView mTitle;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected void injectDependencies() {
        DaggerActivityComponent.builder()
                .applicationComponent(BaseApplication.getAppComponent())
                .build().inject(this);
    }



    @Override
    protected void initData(Bundle savedInstanceState) {
        StartAppSDK.init(this, getString(R.string.startapp_key), true);
        //AdmobUtil.s
        mInterstitialAd = AdmobUtil.loadInterstitial(this, new InterstitialAd(this));
        mPresenter.attachView(this);
        mTitle.setText(getString(R.string.app_name));
        initRecyclerview();
        if(PreferencesUtil.getBoolean(AppConstant.IS_FIRST_TIME, true)) {
            mPresenter.initializeData();
        }
        //getAppList();
        mPresenter.getPaketList();



    }

    @Override
    protected void initWidget() {
        super.initWidget();
        initActionBar();
    }

    @Override
    protected void initVariables() {
        super.initVariables();

    }

    protected void initActionBar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }




    @Override
    protected void onResume() {
        super.onResume();

    }

    private void getAppList() {
        Map<String, String> options = new HashMap<>();
        options.put("category", "prank_on_screen");
        options.put("promoted", "true");
        options.put("limit", "6");
        options.put("random", "true");
        mPresenter.getAppList(options);
    }


    @Override
    public void onGetAppList(List<App> apps) {
        LogUtil.d(TAG, apps.size());
        appList = apps;
        mAppsAdapter.setData(apps);
    }

    @Override
    public void onGetPaketList(List<Paket> paketList) {

        mPaketList = paketList;
        mPaketAdapter.addData(mPaketList);
    }


    /*@Override
    public void onItemClick(int position) {
        AppUtil.goToMarket(appList.get(position).getPackageName());
    }*/


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    private void initRecyclerview() {
        mPaketAdapter = new PaketAdapter(R.layout.adapter_paket, mPaketList);
        mPaketAdapter.setOnItemClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerview.setHasFixedSize(true);
        mRecyclerview.setLayoutManager(linearLayoutManager);
        mRecyclerview.setNestedScrollingEnabled(false);
        mRecyclerview.setAdapter(mPaketAdapter);

    }


    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

         mInterstitialAd.setAdListener(new AdListener() {
             @Override
             public void onAdClosed() {
                 super.onAdClosed();
                 goToSoalActivity(position);
             }
         });

        if(AdmobUtil.increaseAdmobValue() >= AdmobUtil.ARG_TRIGGER_VALUE) {
            AdmobUtil.resetAdmobValue();
            if(mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();Toast.makeText(this, "Loaded", Toast.LENGTH_SHORT).show();
            } else {
                mInterstitialAd = AdmobUtil.loadInterstitial(this, new InterstitialAd(this));
                mInterstitialAd.setAdListener(new AdListener(){
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        goToSoalActivity(position);
                    }

                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mInterstitialAd.show();
                    }
                });
            }
        } else {
            goToSoalActivity(position);
        }

    }

    public void goToSoalActivity(int position) {
        Paket paket = mPaketList.get(position);
        Intent intent = new Intent(HomeActivity.this, SoalActivity.class);
        intent.putExtra("paket", paket);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (!PreferencesUtil.getBoolean(PreferencesUtil.RATE_IT, false)) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

            alertDialogBuilder.setMessage("Jika berkenan, mohon berikan rate bintang 5. \n \n" +
                    "Saya indie developer, rating anda membantu saya bertahan di playstore, terimakasih")
                    .setTitle("Rate 5 Stars")
                    .setPositiveButton("Rate Now", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            PreferencesUtil.putBoolean(PreferencesUtil.RATE_IT, true);
                            AppUtil.goToMarket(getPackageName());
                        }
                    })
                    .setNegativeButton("Later", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            return;
        }
        super.onBackPressed();
    }
}
