package com.testcat.kemnhukham.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.testcat.kemnhukham.R;
import com.testcat.kemnhukham.base.BaseAdapter;
import com.testcat.kemnhukham.data.model.Paket;
import com.testcat.kemnhukham.util.LogUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by kodeartisan on 21/09/17.
 */

public class PaketAdapter extends BaseQuickAdapter<Paket, BaseViewHolder> {

    public static final String TAG = PaketAdapter.class.getSimpleName();


    public PaketAdapter(@LayoutRes int layoutResId, @Nullable List<Paket> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Paket item) {
        helper.setText(R.id.title, item.getTitle());
    }
}
