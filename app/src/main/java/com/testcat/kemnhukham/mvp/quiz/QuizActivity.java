package com.testcat.kemnhukham.mvp.quiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;

import com.google.android.gms.ads.AdView;
import com.testcat.kemnhukham.R;
import com.testcat.kemnhukham.base.BaseActivityMvp;
import com.testcat.kemnhukham.base.BaseApplication;
import com.testcat.kemnhukham.data.model.Soal;
import com.testcat.kemnhukham.di.component.DaggerActivityComponent;
import com.testcat.kemnhukham.util.AdmobUtil;
import com.testcat.kemnhukham.util.LogUtil;

import butterknife.BindView;

public class QuizActivity extends BaseActivityMvp<QuizPresenter> implements QuizContract.View {

    public static final String TAG = QuizActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private Soal mSoal;

    @BindView(R.id.webview)
    WebView mWebview;

    @BindView(R.id.banner_ads)
    AdView mBannerAds;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_quiz;
    }

    @Override
    protected void injectDependencies() {

    }

    @Override
    protected void initWidget() {
        initToolbar();
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        mSoal = getIntent().getParcelableExtra("soal");
        mWebview.loadUrl(mSoal.getUrl());
        mWebview.getSettings().setJavaScriptEnabled(true);
        mBannerAds.loadAd(AdmobUtil.loadBannerView(mBannerAds));
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(mSoal.getTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onPause() {
        if (mBannerAds != null) {
            mBannerAds.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mBannerAds != null) {
            mBannerAds.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mBannerAds != null) {
            mBannerAds.destroy();
        }
        super.onDestroy();
    }
}
