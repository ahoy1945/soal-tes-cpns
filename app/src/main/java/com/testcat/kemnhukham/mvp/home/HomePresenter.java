package com.testcat.kemnhukham.mvp.home;


import android.support.annotation.NonNull;

import com.testcat.kemnhukham.constant.AppConstant;
import com.testcat.kemnhukham.constant.Appkode4Constant;
import com.testcat.kemnhukham.data.model.App;
import com.testcat.kemnhukham.data.model.Paket;
import com.testcat.kemnhukham.data.model.Soal;
import com.testcat.kemnhukham.data.repository.appkode4.app.IAppRepository;
import com.testcat.kemnhukham.rx.RxPresenter;
import com.testcat.kemnhukham.rx.scheduler.BaseSchedulerProvider;
import com.testcat.kemnhukham.util.LogUtil;
import com.testcat.kemnhukham.util.PreferencesUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by kodeartisan on 24/08/17.
 */

public class HomePresenter extends RxPresenter<HomeContract.View> implements HomeContract.Presenter<HomeContract.View> {

    public static final String TAG = HomePresenter.class.getSimpleName();

    private BaseSchedulerProvider mSchedulerProvider;
    private IAppRepository mAppRepository;
    private HomeContract.View mView;

    private List<Paket> mPaketList = new ArrayList<>();

    @Inject
    public HomePresenter(@NonNull BaseSchedulerProvider schedulerProvider, @NonNull IAppRepository appRepository) {
        this.mAppRepository = appRepository;
        this.mSchedulerProvider = schedulerProvider;

    }

    @Override
    public void initializeData() {
        //this.mView.showOnLoading();

        addSubscribe(mAppRepository.get(Appkode4Constant.APP_ID)
                .subscribe(this::processData, this::processError));

    }

    @Override
    public void getAppList(Map<String, String> options) {
        addSubscribe(mAppRepository.getAppList(options)
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(this::processDataAppList, this::processError));
    }

    @Override
    public void getPaketList() {
        mPaketList.clear();
        initPaketList();

        mView.onGetPaketList(mPaketList);


    }

    private void processDataAppList(List<App> apps) {
        mView.onGetAppList(apps);
    }

    private void processError(Throwable throwable) {

        LogUtil.d(TAG, throwable.getMessage());
        //this.mView.showOnError("Error");
    }

    private void processData(App app) {
        PreferencesUtil.putBoolean(AppConstant.IS_FIRST_TIME, false);
       // this.mView.showOnSuccess();
    }

    @Override
    public void attachView(@NonNull HomeContract.View view) {
        super.attachView(view);
        this.mView = view;
    }

    private void initPaketList() {
        List<Soal> soalList1 = new ArrayList<>();
        soalList1.add(new Soal("BAHASA INDONESIA", "file:///android_asset/Indo.html"));
        soalList1.add(new Soal("LOGIKA FORMIL", "file:///android_asset/logika.html"));
        soalList1.add(new Soal("ARITMATIKA", "file:///android_asset/Aritmatika.html"));
        soalList1.add(new Soal("TES SERI", "file:///android_asset/seri.html"));
        soalList1.add(new Soal("ANTONIM", "file:///android_asset/antonim.html"));
        soalList1.add(new Soal("SINONIM", "file:///android_asset/sinonim.html"));
        soalList1.add(new Soal("TATA NEGARA", "file:///android_asset/tatanegara.html"));
        soalList1.add(new Soal("PADANANAN DAN HUBUNGAN", "file:///android_asset/padanan.html"));
        soalList1.add(new Soal("FALSAFAH", "file:///android_asset/falsafah.html"));

        List<Soal> soalList2 = new ArrayList<>();
        soalList2.add(new Soal("BAHASA INDONESIA", "file:///android_asset/bindob.html"));
        soalList2.add(new Soal("TATA NEGARA", "file:///android_asset/btatanegarab.html"));
        soalList2.add(new Soal("BAHASA INGGRIS", "file:///android_asset/binggris.html"));
        soalList2.add(new Soal("KEBIJAKAN PEMERINTAH", "file:///android_asset/bkebijakan.html"));
        soalList2.add(new Soal("TES READING", "file:///android_asset/breading.html"));
        soalList2.add(new Soal("UUD 1945 & AMANDEMEN", "file:///android_asset/bUUD.html"));
        soalList2.add(new Soal("SEJARAH", "file:///android_asset/bsejarah.html"));
        soalList2.add(new Soal("STRUCTURE (BAHASA INGGRIS)", "file:///android_asset/bsturktur.html"));

        List<Soal> soalList3 = new ArrayList<>();
        soalList3.add(new Soal("WAWASAN KEBANGSAAN A", "file:///android_asset/ctwk.html"));
        soalList3.add(new Soal("WAWASAN KEBANGSAAN B", "file:///android_asset/ctwk2.html"));
        soalList3.add(new Soal("INTELEJENSI UMUM (BAHASA INDONESIA)", "file:///android_asset/cpadanan.html"));
        soalList3.add(new Soal("TES KARAKTERISTIK PRIBADI A", "file:///android_asset/ctesk.html"));
        soalList3.add(new Soal("TES KARAKTERISTIK PRIBADI B", "file:///android_asset/cpribadi.html"));
        soalList3.add(new Soal("TES INTELEGENSI UMUM (TIU) LOGIKA", "file:///android_asset/cintelegen.html"));

        List<Soal> soalList4 = new ArrayList<>();
        soalList4.add(new Soal("TES KEPRIBADIAN A", "file:///android_asset/dpribadi.html"));
        soalList4.add(new Soal("TES KEPRIBADIAN B", "file:///android_asset/dpribadib.html"));
        soalList4.add(new Soal("WAWASAN KEBANGSAAN A", "file:///android_asset/dtwk.html"));
        soalList4.add(new Soal("WAWASAN KEBANGSAAN B", "file:///android_asset/dtwkb.html"));
        soalList4.add(new Soal("WAWASAN KEBANGSAAN C", "file:///android_asset/dtwkc.html"));
        soalList4.add(new Soal("TES INTELEGENSI UMUM (TIU) LOGIKA", "file:///android_asset/dintelegen.html"));

        List<Soal> soalList5 = new ArrayList<>();
        soalList5.add(new Soal("TES KECERDASAN UMUM", "file:///android_asset/TKU.html"));
        soalList5.add(new Soal("TES KECEPATAN DAN KETELITIAN KLERIKAL", "file:///android_asset/kecepatan.html"));
        soalList5.add(new Soal("TES KECEPATAN PERBEDAAN", "file:///android_asset/kecepatanperbedaan.html"));
        soalList5.add(new Soal("SINONIM TINGKAT LANJUT", "file:///android_asset/sinonim1.html"));
        soalList5.add(new Soal("ANALOGI", "file:///android_asset/Analogi.html"));

        mPaketList.add(new Paket("CPNS KEMENKUMHAM (A)", soalList1));
        mPaketList.add(new Paket("CPNS KEMENKUMHAM (B)", soalList2));
        mPaketList.add(new Paket("CPNS KEMENKUMHAM (C)", soalList3));
        mPaketList.add(new Paket("CPNS KEMENKUMHAM (D)", soalList4));
        mPaketList.add(new Paket("CPNS KEMENKUMHAM (E)", soalList5));
    }
}
